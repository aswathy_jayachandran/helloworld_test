package is.hi.cs.junit4demo;

public class Money {
	  private int amount;

	  public Money(int amount) {
	    this.amount = amount;
	  }

	  public int getAmount() {
	    return amount;
	  }

	  public Money add(Money m) {
	    return new Money(amount + m.getAmount());
	  }

	  public boolean equals(Object obj) {
	    if (obj instanceof Money) {
	      return (amount == ((Money) obj).getAmount());
	    } else
	      return false;
	  }

	  public String toString() {
	    return Integer.toString(amount);
	  }
	}
